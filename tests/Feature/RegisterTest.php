<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * Register user
     *
     * @return void
     */
    public function testRegister()
    {
        $password = $this->faker->password;
        $params = [
            'name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'password' => $password,
            'password_confirmation' => $password
        ];
        $response = $this->post('/api/register', $params);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'name' => $params['name'],
                'email' => $params['email']
            ]
        ]);
        $this->assertDatabaseHas(
            'users',
            [
                'name' => $params['name'],
                'email' => $params['email']
            ]
        );
    }

    /**
     * Register user using existing email
     *
     * @return void
     */
    public function testRegisterEmailExist()
    {
        $user = factory(User::class)->create();

        $password = $this->faker->password;
        $params = [
            'name' => $this->faker->name,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password
        ];
        $response = $this->post('/api/register', $params, [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(422);
    }
}
